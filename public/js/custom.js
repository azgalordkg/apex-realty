$(document).ready(function () {


    // Function PooPup
    $('.popup-with-move-anim').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom'
    });


    /* Custom select design */
    $('.drop-down').append('<div class="button"></div>');
    $('.drop-down').append('<ul class="select-list"></ul>');
    $('.drop-down select option').each(function () {
        var bg = $(this).css('background-image');
        $('.select-list').append('<li class="clsAnchor"><span value="' + $(this).val() + '" class="' + $(this).attr('class') + '" style=background-image:' + bg + '>' + $(this).text() + '</span></li>');
    });
    $('.drop-down .button').html('<span style=background-image:' + $('.drop-down select').find(':selected').css('background-image') + '>' + $('.drop-down select').find(':selected').text() + '</span>' + '<a href="javascript:void(0);" class="select-list-link"><span class="lnr lnr-chevron-down"></span></a>');
    $('.drop-down ul li').each(function () {
        if ($(this).find('span').text() == $('.drop-down select').find(':selected').text()) {
            $(this).addClass('active');
        }
    });
    $('.drop-down .select-list span').on('click', function () {
        var dd_text = $(this).text();
        var dd_img = $(this).css('background-image');
        var dd_val = $(this).attr('value');
        $('.drop-down .button').html('<span style=background-image:' + dd_img + '>' + dd_text + '</span>' + '<a href="javascript:void(0);" class="select-list-link"><span class="lnr lnr-chevron-down"></span></a>');
        $('.drop-down .select-list span').parent().removeClass('active');
        $(this).parent().addClass('active');
        $('.drop-down select[name=options]').val(dd_val);
        $('.drop-down .select-list li').slideUp();
    });
    $('.drop-down .button').on('click', 'a.select-list-link', function () {
        $('.drop-down ul li').slideToggle();
    });
    /* End */

    // var node = document.createElement("span");                 // Create a <li> node
    // var textnode = document.createTextNode("8%");         // Create a text node
    // node.appendChild(textnode);                              // Append the text to <li>
    // document.getElementsByClassName("ui-slider-handle").appendChild(node);     // Append <li> to <ul> with id="myList"
    //
    // var parent = document.createElement("ui-slider-handle");
    // var p = document.createElement("p");
    // parent.append(p);


    $('.mobile__menu__icon').on('click', function () {
            $('body').toggleClass('open');
            $('.menu').toggleClass('open');
            $('.mobile__menu__icon').toggleClass('open');
        }
    );


    $('.HeaderLink').on('click', function () {
        console.log(1);
        $('body').removeClass('open');
        $('.menu').removeClass('open');
        $('.mobile__menu__icon').removeClass('open');

    })


});