import axios from '../axios-apex';

export const GET_SUCCESS = 'GET_SUCCESS';
export const GET_FAILURE = 'GET_FAILURE';

export const GET_STEPS = 'GET_STEPS';
export const CHANGE_SELECTED_VALUES = 'CHANGE_SELECTED_VALUES';
export const GET_UPDATE_BUTTON_ACTIVE = 'GET_UPDATE_BUTTON_ACTIVE';
export const GET_SORTED_LIST = 'GET_SORTED_LIST';

export const GET_VALUES_FOR_TRIANGLE = 'GET_VALUES_FOR_TRIANGLE';
export const GET_VALUES_FROM_MULTI_SELECTS = 'GET_VALUES_FROM_MULTI_SELECTS';
export const CARDS_HIDDEN_HANDLER = 'CARDS_HIDDEN_HANDLER';

export const getSuccess = response => ({type: GET_SUCCESS, response});
export const getFailure = error => ({type: GET_FAILURE, error});

export const getSteps = steps => ({type: GET_STEPS, steps});
export const changeSelectedValues = selected => ({type: CHANGE_SELECTED_VALUES, selected});
export const getUpdateButtonActive = () => ({type: GET_UPDATE_BUTTON_ACTIVE});
export const getSortedList = sorted => ({type: GET_SORTED_LIST, sorted});

export const getValuesForTriangle = activeCard => ({type: GET_VALUES_FOR_TRIANGLE, activeCard});
export const getValuesFromMultiSelects = selectedObject => ({type: GET_VALUES_FROM_MULTI_SELECTS, selectedObject});
export const cardsHiddenHandler = () => ({type: CARDS_HIDDEN_HANDLER});

export const fetchMainData = () => {
  return dispatch => {
    axios.get('').then(
      response => dispatch(getSuccess(response.data.feed.entry)),
      error => dispatch(getFailure(error)),
    );
  }
};