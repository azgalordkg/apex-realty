import {
  CARDS_HIDDEN_HANDLER,
  CHANGE_SELECTED_VALUES,
  GET_FAILURE, GET_SORTED_LIST,
  GET_STEPS,
  GET_SUCCESS,
  GET_UPDATE_BUTTON_ACTIVE, GET_VALUES_FOR_TRIANGLE, GET_VALUES_FROM_MULTI_SELECTS
} from "./apexActions";
import {initSteps} from "../constants";

let initialState = { // temporary saving store to localStorage
  list: null,
  error: '',
  steps: initSteps,
  selected: {
    City: [], // Old value was 'All'
    Area: [], // Old value was 'All'
    Downpayment: 'Max Downpayment',
    Units: [],
  },
  ifUpdate: false,
  sortedList: null,
  activeCard: null,
  activeROI: null,
  cardsHidden: false,
};

const apexReducer = (state = initialState, action) => {
  switch (action.type) {
    case (GET_SUCCESS):
      return {...state, list: action.response,};
    case (GET_FAILURE):
      return {...state, error: action.error,};
    case GET_STEPS:
      return {...state, steps: action.steps,};
    case CHANGE_SELECTED_VALUES:
      return {...state, selected: action.selected,};
    case GET_UPDATE_BUTTON_ACTIVE:
      return {...state, ifUpdate: true};
    case GET_SORTED_LIST:
      return {...state, sortedList: action.sorted};
    case GET_VALUES_FOR_TRIANGLE:
      if (state.sortedList.length > action.activeCard) {
        return {
          ...state,
          activeCard: action.activeCard,
          activeROI: state.sortedList[action.activeCard].gsx$roi.$t
        };
      }
      return state;
    case GET_VALUES_FROM_MULTI_SELECTS:
      return {
        ...state,
        selected: {...state.selected,
          [action.selectedObject.item] : action.selectedObject.dropdownValues
        }
      };
    case CARDS_HIDDEN_HANDLER:
      return {...state, cardsHidden: !state.cardsHidden};
    default:
      return state;
  }
};

export default apexReducer;