import React, {Component} from 'react';
import './DownPaymentCards.css';
// Import components
import DownPaymentCard from "../DownPaymentCard/DownPaymentCard";
import Container from "../../components/UI/Container/Container";
import TriangleWrapper from '../../components/UI/TriangleWrapper/TriangleWrapper';
// Import redux components
import {connect} from "react-redux";
import {fetchMainData, getSortedList, getValuesForTriangle} from "../../store/apexActions";

class DownPaymentCards extends Component {
  state = {
    ifTriangleFixed: false,
    absolutePosition: null, // Absolute position of Triangle
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const cards = document.getElementsByClassName('DownPaymentCard'); // here we get all cards DOM
    // const clientWidth = window.innerWidth;

    if (this.props.activeCard === null && cards.length) { //  && clientWidth > 992 - this checking if we want to disable fixed triangle on mobile displays
      this.checkPositionsOfCardsAndTriangle();
    }
  }

  // Function for check position of cards and change triangle's position

  checkPositionsOfCardsAndTriangle = () => {
    // variables for choose active card
    const downPaymentCardsOffset = document.getElementById('downPaymentCards').offsetTop;
    // variables for making sticky triangle
    const triangle = document.getElementById('triangle');
    let triangleOffset = triangle.offsetHeight;

    window.onscroll = () => {
      this.scrollFunction(downPaymentCardsOffset, triangleOffset);
    };
  };

  // Scroll Function

  scrollFunction = (downPaymentCardsOffset, triangleOffset) => {
    let activeCard = 0;
    const featuresBlockOffset = document.getElementById('Features').offsetTop;
    const documentScroll = document.documentElement.scrollTop || window.pageYOffset;
    // here we checking if one card is active and has table-visible
    const heightOfDisplay = document.documentElement.clientHeight; // this property is necessary for get center of display for making cards active
    const cardsHtml = document.getElementsByClassName('DownPaymentCard');
    const downPaymentCardsHeight = document.getElementById('downPaymentCards').offsetHeight;

    for (let i = 0; i < cardsHtml.length; i++) { // here we finding if card is before the eyes of user and card has to be active.
      if ((cardsHtml[i].offsetTop + downPaymentCardsOffset - (heightOfDisplay / 4)) <= (documentScroll)) activeCard = i;
    }

    // this is part of function for make Triangle sticky
    if (documentScroll <= downPaymentCardsOffset - triangleOffset - 40) { // this part is for make triangle static if we scrolls top-over HowItWorks block
      this.setState({ifTriangleFixed: false});
    } else if (documentScroll >= featuresBlockOffset - triangleOffset - 40) { // this is part of function for make Triangle static if we have scrolled for Features block
      this.setState({ifTriangleFixed: false, absolutePosition: `${downPaymentCardsHeight - triangleOffset - 40}px`});
    } else if (!this.state.ifTriangleFixed) { // and this part of code is for make triangle fixed.
      this.setState({ifTriangleFixed: true, absolutePosition: null});
    }

    //this is part of function for choose active card
    if (activeCard < 0 || activeCard === -0) activeCard = 0;
    if (this.props.activeCard !== activeCard) {
      this.props.getValuesForTriangle(activeCard);
    }
  };

  // Function for getting value without percents

  getValueWithoutPercents = value => {
    return value.split('').map(valueItem => valueItem === '%' ? '' : valueItem).join('');
  };

  // Function for getting series for PieChart in DownPaymentCard

  getSeries = listItem => {
    const cashFlow = parseInt(this.getValueWithoutPercents(listItem.gsx$cf.$t));
    const equity = parseInt(this.getValueWithoutPercents(listItem.gsx$eq.$t));
    const expenses = parseInt(this.getValueWithoutPercents(listItem.gsx$exp_2.$t)) + parseInt(this.getValueWithoutPercents(listItem.gsx$int.$t));

    return [cashFlow, equity, expenses];
  };

  render() {
    return (
      <div style={{display: this.props.cardsHidden ? 'none' : 'block'}} id="downPaymentCards" className="DownPaymentCards card__block">
        {this.props.sorted ? <TriangleWrapper
          absolutePosition={this.state.absolutePosition}
          roi={this.props.activeROI}
          fixed={this.state.ifTriangleFixed}/> : null}
        <Container>
          {this.props.sorted ? <div className="DownPaymentBanner"/> : null}
          <div className="DownPaymentCardsBlock">
            {this.props.sorted ? this.props.sorted.length ? this.props.sorted.map((listItem, index) => {
              return (
                <DownPaymentCard
                  active={this.props.activeCard === index}
                  onInquireClick={(event) => this.props.openModalWithForm(event, listItem)}
                  key={index} listItem={listItem} series={this.getSeries(listItem)}
                  itemIndex={index}
                />
              )
            }) : <div className="ResultsWillBe">No results...</div> : <div className="ResultsWillBe">The list is updated daily</div>}
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  list: state.list,
  selected: state.selected,
  sorted: state.sortedList,
  activeCard: state.activeCard,
  activeROI: state.activeROI,
  cardsHidden: state.cardsHidden,
});

const mapDispatchToProps = dispatch => ({
  fetchMainData: () => dispatch(fetchMainData()),
  getValuesForTriangle: (activeCard) => dispatch(getValuesForTriangle(activeCard)),
  getSortedList: sorted => dispatch(getSortedList(sorted)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DownPaymentCards);