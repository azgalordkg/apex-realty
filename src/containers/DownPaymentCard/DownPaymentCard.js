import React, {Component} from 'react';
import './DownPaymentCard.css';
// Import components and functions
import {Cell, Legend, Pie, PieChart} from "recharts";
import DownPaymentCardTable from "../../components/DownPaymentCardTable/DownPaymentCardTable";
import {returnROI} from "../../constants";
// Import redux components
import {getSortedList} from "../../store/apexActions";
import {connect} from "react-redux";

const COLORS = ['#43951f', '#a2d06e', '#bf0202'];
const RADIAN = Math.PI / 180;

class DownPaymentCard extends Component {
  state = {
    totalIncome: true,
    expenses: true,
    mort: true,
  };

  renderCustomizedLabel = ({cx, cy, midAngle, innerRadius, outerRadius, percent,}) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return this.props.listItem.gsx$downpay.$t !== nextProps.listItem.gsx$downpay.$t
      || this.props.active !== nextProps.active
      || this.props.listItem.tableOpen === 'open'
      || this.props.listItem.tableOpen === 'close';
  }

  // Function for open CashFlowAnalysis

  CashFlowAnalysisOpenTable = index => {
    const sorted = [...this.props.sorted];

    if (sorted[index].tableOpen && sorted[index].tableOpen !== 'close') {
      sorted[index].tableOpen = 'close';
    } else {
      sorted.map(item => item.tableOpen = false); // here we making all value false as default values
      sorted[index].tableOpen = 'open';
    }

    this.setState({totalIncome: true, expenses: true, mort: true,}); // here we update values for table blocks for make all visible
    this.props.getSortedList(sorted); // here we save new sorted list with opened card
  };

  // Function for open or close some block in table in Card

  plusMinusClick = event => {
    const thisId = event.target.id; // this is Id of button for hide or show some block in table
    const checking = thisId.split('').reverse().join(''); // we prepare our id for check if it has "-"
    if (checking[0] !== '-') { // here if we don't have "-" we open block in table
      this.setState({[thisId] : true});
    } else { // else close it
      const idWithoutMinus = (event.target.id).split('').map(val => val === '-' ? '' : val).join('');
      this.setState({[idWithoutMinus] : false});
    }
  };

  render() {
    const data = [
      {name: 'Cash Flow (%)', value: this.props.series[0]},
      {name: 'Equity (%)', value: this.props.series[1]},
      {name: 'Expenses (%)', value: this.props.series[2]},
    ];
    const listItem = this.props.listItem;

    return (
      <div className={`DownPaymentCard ${this.props.active ? 'Active' : null} tables__wrapper`}>
        <div className="DownPaymentCardMainInfo">
          <div className={`DownPaymentCardROI DownPaymentCardItem ${listItem.gsx$score.$t}`}>
            <div>
              <p>{returnROI(listItem.gsx$roi.$t)}</p>
              <span>% ROI</span>
            </div>
            <div className={`DownPaymentCardAAA ${listItem.gsx$score.$t}`}>
              <i>{listItem.gsx$score.$t}</i>
            </div>
          </div>
          <div className="DownPaymentCardTextInfo DownPaymentCardItem">
            <p>
              <b>Down Payment</b>
              <span>{listItem.gsx$downpay.$t}</span>
            </p>
            <p>
              <b>Area</b>
              <span>{listItem.gsx$area.$t}</span>
            </p>
            <p>
              <b>Units</b>
              <span>{listItem.gsx$units.$t}</span>
            </p>
            <p>
              <b>Annual Total Income</b>
              <span>{listItem.gsx$incperyear.$t}</span>
            </p>
            <p>
              <b>Monthly Profit</b>
              <span>{listItem.gsx$permonth.$t}</span>
            </p>
            <p>
              <b>Annual Equity</b>
              <span>{listItem.gsx$principal.$t}</span>
            </p>
          </div>
          <div className="DownPaymentCardChart DownPaymentCardItem">
            <PieChart width={260} height={260}>
              <Pie
                isAnimationActive={false}
                data={data} cx={130} cy={130}
                labelLine={false} label={this.renderCustomizedLabel}
                outerRadius={105} fill="#8884d8" dataKey="value"
              >
                {data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]}/>)}
              </Pie>
              <Legend verticalAlign="bottom" height={20}/>
            </PieChart>
          </div>
        </div>
        <div className="DownPaymentCardButtons">
          <button
            className="DownPaymentCardButton CashFlowAnalysis"
            onClick={() => this.CashFlowAnalysisOpenTable(this.props.itemIndex)}>
            <span>Cash Flow Analysis</span>
          </button>
          <button
            className="DownPaymentCardButton Inquire"
            onClick={this.props.onInquireClick}>
            <span>inquire</span>
          </button>
        </div>
        <DownPaymentCardTable
          plusMinusClick={this.plusMinusClick}
          listItem={listItem} visible={listItem.tableOpen}
          totalIncomeVisible={this.state.totalIncome}
          expensesVisible={this.state.expenses}
          mortVisible={this.state.mort}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  sorted: state.sortedList,
});

const mapDispatchToProps = dispatch => ({
  getSortedList: sorted => dispatch(getSortedList(sorted)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DownPaymentCard);