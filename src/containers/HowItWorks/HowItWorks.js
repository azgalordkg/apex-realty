import React, {Component} from 'react';
import './HowItWorks.css';
// Import components
import StepsIndicator from "../../components/StepsComponents/StepsIndicator/StepsIndicator";
import StepDropDowns from "../../components/StepsComponents/StepDropDowns/StepDropDowns";
import StepButtons from "../../components/StepsComponents/StepButtons/StepButtons";
import CardsPreloader from "../../components/UI/CardsPreloader/CardsPreloader";
// Import redux components
import {connect} from "react-redux";
import {
  cardsHiddenHandler,
  changeSelectedValues,
  fetchMainData,
  getSortedList,
  getSteps,
  getUpdateButtonActive, getValuesFromMultiSelects
} from "../../store/apexActions";
import {getValueWithCommas} from "../../constants";
// main constants
import {initSteps} from "../../constants";

const multiSelectValues = {
  multiSelect1: null,
  multiSelect2: null,
};

const multiSelectAllValues = [true, true];

class HowItWorks extends Component {
  state = {
    activeStep: 1,
    multiSelect1: [{label: 'All', id: 0},],
    multiSelect2: [{label: 'All', id: 0},],
    multiSelectUnits: [{label: 'All Units', id: 0}, {label: '0 - 5', id: 1}, {label: '6 - 10', id: 2}, {label: '11+', id: 3}],
    activeMultiSelect: null,
  };

  componentDidMount() {
    this.props.fetchMainData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.list !== prevProps.list) {
      this.getValuesFromDropdownsToState();
    }

    if (this.state.activeMultiSelect !== null) { // we need this if-else checking for saving only info for active multiSelect
      multiSelectValues[`multiSelect${this.state.activeMultiSelect}`] = prevState[`multiSelect${this.state.activeMultiSelect}`];
    } else {
      multiSelectValues.multiSelect1 = prevState.multiSelect1;
      multiSelectValues.multiSelect2 = prevState.multiSelect2;
    }
  }

  // Function for get values for dropdowns in block Steps in component HowItWorks

  getValuesFromDropdownsToState = () => {
    // Here main constants of values for getting them to constant steps (copy of steps it state).
    const list = this.props.list;
    const cities = list.map(item => item.gsx$city.$t).filter((item, pos, self) => self.indexOf(item) === pos).sort();
    const areas = list.map(item => item.gsx$area.$t).filter((item, pos, self) => self.indexOf(item) === pos).sort();
    const downPaymentsSteps = [];
    // let downPayments = list.map(item => item.gsx$downpay.$t).map(value => parseFloat(value.split('') old value
    //   .map(val => val === '$' || val === ',' ? '' : val)
    //   .join(''))).sort((a, b) => a - b);
    // downPayments = downPayments.map(value => isNaN(value) ? 0 : value).sort((a,b) => a - b); // here we checking if all values are numbers - old value
    // const tithing = downPayments[downPayments.length - 1] / 10; old value

    for (let i = 50000; i < 500000; i += 50000) { // here we push all values by 50.000 $
      downPaymentsSteps.push(i);
    }
    for (let i = 500000; i < 1000000; i += 100000) { // here we push all values by 50.000 $
      downPaymentsSteps.push(i);
    }
    for (let i = 1000000; i < 2000000; i += 250000) { // here we push all values by 50.000 $
      downPaymentsSteps.push(i);
    }
    for (let i = 2000000; i < 5000000; i += 500000) { // here we push all values by 50.000 $
      downPaymentsSteps.push(i);
    }
    for (let i = 5000000; i < 11000000; i += 1000000) { // here we push all values by 50.000 $
      downPaymentsSteps.push(i);
    }
    // for (let i = 1000000; i < downPayments[downPayments.length - 1] - 1000000; i += 1000000) { // here we push all values by 1.000.000 $
    //   downPaymentsSteps.push(i);
    // }
    // downPaymentsSteps.push(downPayments[downPayments.length - 1]); // here we push maximal value

    // Here copies of state.
    const multiSelect1 = [...this.state.multiSelect1];
    const multiSelect2 = [...this.state.multiSelect2];
    const steps = initSteps;
    const step1dropdowns = [...steps[0]];
    const step2dropdowns = [...steps[1]];

    // here we getting values to copies of state.
    cities.forEach((city, index) => {
      multiSelect1.push({label: city, id: index + 1});
      step1dropdowns[0].content.push(city);
    });
    areas.forEach((area, index) => {
      multiSelect2.push({label: area, id: index + 1});
      step1dropdowns[1].content.push(area);
    });
    downPaymentsSteps.forEach(downPayment => {
      step2dropdowns[0].content.push(getValueWithCommas(downPayment));
    });
    steps[0].dropdowns = step1dropdowns;

    // and here we setting new state and store values.
    this.setState({multiSelect1, multiSelect2});
    this.props.getSteps(steps);
  };

  // Function for sorting items by selected cities

  sortAreasBySelectedCities = selectedCities => {
    const list = this.props.list;
    const multiSelect2 = [{label: 'All', id: 0},];
    const steps = initSteps;
    const step1dropdowns = [...steps[0]];
    let areas = list.map(item => item.gsx$area.$t).filter((item, pos, self) => self.indexOf(item) === pos).sort();

    if (selectedCities[0] !== 'All' && selectedCities.length) {
      let newAreas = [];
      for (let i = 0; i < selectedCities.length; i++) {
        list.forEach(item => {
          if (item.gsx$city.$t === selectedCities[i]) newAreas.push(item.gsx$area.$t);
        });
      }
      areas = newAreas.filter((item, pos, self) => self.indexOf(item) === pos).sort();
    }

    areas.forEach((area, index) => {
      multiSelect2.push({label: area, id: index + 1});
      step1dropdowns[1].content.push(area);
    });

    // and here we setting new state values.
    this.setState({multiSelect2});
    steps[0].dropdowns = step1dropdowns;
    this.props.getSteps(steps);
  };

  // Function for changing selects of Steps

  onSelectChange = event => {
    const selected = {...this.props.selected};
    let value = event.target.value;
    if (this.state.activeStep === 2 && value !== 'All') {
      value = parseInt(value.split('').map(val => val === ',' ||  val === '$' ? '' : val).join(''));
    }
    selected[event.target.name] = value;
    this.props.changeSelectedValues(selected);
  };

  // Function for check if buttons should be disabled

  checkIfButtonsDisabled = () => {
    // step - this active step.
    let disabled = false;

    if (this.state.activeStep === 1) { // this checking is for check if this step contains multiSelect dropdowns
      disabled = !this.props.selected.City.length || !this.props.selected.Area.length;
    } else if (this.state.activeStep === 3) {
      disabled = !this.props.selected.Units.length;
    } else {
      disabled = this.props.selected.Downpayment === 'Max Downpayment';
      // const thisStep = [...this.props.steps[this.state.activeStep - 1]];
      // thisStep.forEach(item => disabled = (this.props.selected[item.name] === item.name) ? true : disabled);
    }
    return disabled;
  };

  // Functions for go to the next or prev steps

  goNext = () => {
    if (this.state.activeStep !== 3) this.setState({activeStep: this.state.activeStep + 1});
  };

  goBack = () => {
    if (this.state.activeStep !== 1) this.setState({activeStep: this.state.activeStep - 1});
  };

  // Function for get changed downpayment value

  getCorrectDownpaymentValue = value => {
    if (value !== 'All') {
      return parseFloat(value.split('').map(val => (val !== '$' && val !== ',') ? val : '').join(''));
    }
    return value;
  };

  // Function for getting list after sort by selected values

  sortList = () => {
    const list = this.props.list;
    const selected = this.props.selected;
    const city = selected.City;
    const area = selected.Area;
    const downpayment = selected.Downpayment;
    const units = selected.Units;
    const sortedList = [];

    list.forEach(listItem => {
      const listDownpayment = this.getCorrectDownpaymentValue(listItem.gsx$downpay.$t);
      const listUnits = parseFloat(listItem.gsx$units.$t);
      const listCity = listItem.gsx$city.$t;
      const listArea = listItem.gsx$area.$t;

      let checkingCity = city[0] === 'All';
      let checkingArea = area[0] === 'All';
      let checkingDownpayment = downpayment === 'All';
      let checkingUnits = units[0] === 'All Units';

      // checking for Cities
      if (!checkingCity) city.forEach(cityVal => cityVal === listCity ? checkingCity = true : null);
      // checking for Areas
      if (!checkingArea) area.forEach(areaVal => areaVal === listArea ? checkingArea = true : null);
      // checking for Downpayment
      if (!checkingDownpayment && listDownpayment <= downpayment) checkingDownpayment = true;
      // checking for Units
      if (!checkingUnits) {
        units.forEach(unitsVal => {
          if (unitsVal === '11+' && listUnits >= 11) {
            checkingUnits = true;
          } else {
            unitsVal = unitsVal.split(' - ');
            if (listUnits >= parseInt(unitsVal[0]) && listUnits <= parseInt(unitsVal[1])) checkingUnits = true;
          }
        });
      }

      if (checkingArea && checkingDownpayment && checkingUnits && checkingCity) {
        listItem.tableOpen = false; // here we get default value for open and close table with data
        sortedList.push(listItem);
      }
    });
    this.props.getSortedList(sortedList);
    this.props.getUpdateButtonActive();
  };

  // Function for checking if All Values true and we want to remove All values

  checkIfAllValuesTrue = (optionsList, index) => {
    const thisCopyOfValues = multiSelectValues[`multiSelect${index + 1}`]; // copy of this multiSelectValues
    if (thisCopyOfValues[0].value && !optionsList[0].value) { // here we check that All value have been chosen but All value not chosen.
      return optionsList.map(optValue => ({...optValue, value: false}));
    }
    return optionsList;
  };

  // Function for check if we have only one false value in Array.

  checkIfOneFalse = optionsList => {
    let check = 0;
    optionsList.forEach(optValue => !optValue.value ? check++ : null);
    return check === 1;
  };

  // Functions for getting values for City and Area dropdowns.

  optionClicked(index, optionsList) {
    const optionsListCopy = [...optionsList];
    // index - number of dropdown for changing value in state, optionsList - list of dropdown items
    optionsList = this.checkIfAllValuesTrue(optionsList, index);
    if (optionsList[0].value && this.checkIfOneFalse(optionsListCopy) && multiSelectAllValues[index]) {
      optionsList[0].value = false;
      multiSelectAllValues[index] = false;
    }

    this.setState({ [`multiSelect${index + 1}`]: optionsList });
    this.prepareMultiSelectValuesForStore(optionsList, index);
  }

  selectedBadgeClicked(index, optionsList) {
    this.setState({activeMultiSelect : index + 1});
    const optionsListCopy = [...optionsList];
    // index - number of dropdown for changing value in state, optionsList - list of dropdown items
    optionsList = this.checkIfAllValuesTrue(optionsList, index);

    if (optionsList[0].value && this.checkIfOneFalse(optionsListCopy) && multiSelectAllValues[index]) {
      optionsList[0].value = false;
      multiSelectAllValues[index] = false;
    } else if (optionsList[0].value) { // this code makes all values active if user chooses All-value.
      optionsList = optionsList.map(optValue => ({...optValue, value: true}));
      multiSelectAllValues[index] = true;
    }

    this.setState({ [`multiSelect${index + 1}`]: optionsList });
    this.prepareMultiSelectValuesForStore(optionsList, index);
  }

  // Function for preparing values about multiSelect dropdowns for setting them to store

  prepareMultiSelectValuesForStore = (optionsList, index) => {
    let dropdownValues = [];
    if (optionsList[0].value) {
      dropdownValues.push(optionsList[0].label);
    } else {
      optionsList.forEach(item => item.value ? dropdownValues.push(item.label) : null);
    }
    if (!index) this.sortAreasBySelectedCities(dropdownValues);

    if (index === 'Units') {
      this.props.getValuesFromMultiSelects({item: 'Units', dropdownValues});
    } else if (index) {
      this.props.getValuesFromMultiSelects({item: 'Area', dropdownValues});
    } else {
      this.props.getValuesFromMultiSelects({item: 'City', dropdownValues});
    }
  };

  // Functions for getting values for Units Dropdown.

  optionClickedUnits(optionsList) {
    if (optionsList[0].value) { // here we checking if user chooses All Units value
      optionsList = optionsList.map((optVal, index) => index ? ({...optVal, value: false}) : optVal);
    }
    this.setState({ multiSelectUnits : optionsList });
    this.prepareMultiSelectValuesForStore(optionsList, 'Units');
  }

  selectedBadgeClickedUnits(optionsList) {
    if (optionsList[0].value) { // here we checking if user chooses All Units value
      optionsList = optionsList.map((optVal, index) => index ? ({...optVal, value: false}) : optVal);
    }
    this.setState({ multiSelectUnits : optionsList });
    this.prepareMultiSelectValuesForStore(optionsList, 'Units');
  }

  render() {
    return (
      <div id="howItWorks" className="HowItWorks how__it__works">
        <span onClick={this.props.cardsHiddenHandler} className="red_arrow"/>
        <div className="container">
          <h3>How It Works</h3>
          <span className="white__dash"/>
          <p>Apex Realty Investments (ARI) is a unique tool that makes the <br/> search for a profitable investment as easy as:</p>
          <div className="steps__block">
            <StepsIndicator activeStep={this.state.activeStep}/>
            {this.props.list ? <StepDropDowns
              change={this.onSelectChange}
              steps={this.props.steps[this.state.activeStep - 1]}
              selected={this.props.selected}
              optionClicked={index => this.optionClicked.bind(this, index)}
              selectedBadgeClicked={index => this.selectedBadgeClicked.bind(this, index)}
              multiSelect1={this.state.multiSelect1}
              multiSelect2={this.state.multiSelect2}
              // here props for Third Step
              multiSelectUnits={this.state.multiSelectUnits}
              optionClickedUnits={this.optionClickedUnits.bind(this)}
              selectedBadgeClickedUnits={this.selectedBadgeClickedUnits.bind(this)}
            /> : <CardsPreloader/>}
            <StepButtons
              step={this.state.activeStep}
              prevClick={this.goBack} nextClick={this.goNext}
              update={this.props.update}
              ifDisabled={this.checkIfButtonsDisabled()}
              findClick={this.sortList}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  list: state.list,
  steps: state.steps,
  selected: state.selected,
  update: state.ifUpdate,
});

const mapDispatchToProps = dispatch => ({
  fetchMainData: () => dispatch(fetchMainData()),
  getSteps: steps => dispatch(getSteps(steps)),
  changeSelectedValues: selected => dispatch(changeSelectedValues(selected)),
  getUpdateButtonActive: () => dispatch(getUpdateButtonActive()),
  getSortedList: sorted => dispatch(getSortedList(sorted)),
  getValuesFromMultiSelects: selectedObject => dispatch(getValuesFromMultiSelects(selectedObject)),
  cardsHiddenHandler: () => dispatch(cardsHiddenHandler()),
});

export default connect(mapStateToProps, mapDispatchToProps)(HowItWorks);