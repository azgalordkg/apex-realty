// function for return correct value of ROI without percents

export const returnROI = ROI => {
  const changedROI = parseFloat(ROI.split('').map(value => value === '%' ? '' : value).join(''));
  return Number.isInteger(changedROI) ? changedROI + '.0' : changedROI;
};

export const initSteps = [
  [
    {name: 'City', content: ['City', 'All',  'Montreal']},
    {name: 'Area', content: ['Area', 'All', ]},
  ],
  [
    {name: 'Downpayment', content: ['Max Downpayment', 'All', ]},
  ],
  [
    {name: 'Units', content: ['Units', 'All Units', '0 - 4', '5 - 10', '11+']},
  ],
];

// function for return AAA by ROI

export const returnAAA = ROI => {
  if (ROI) {
    const intROI = parseFloat(returnROI(ROI));
    if (intROI < 4) {
      return 'A';
    } else if (intROI < 6) {
      return 'AA';
    } else {
      return 'AAA';
    }
  }
  return 'AAA';
};

// constants for forms

export const mainForm = {
  name: '',
  email: '',
  units: '',
  price: '',
  income: '',
  fullyRented: false,
};

export const additionalForm = {
  units: '',
  area: '',
  firstName: '',
  lastName: '',
  email: '',
  phone: '',
};

// Functions for getting value with commas

export const getValueWithCommas = word => {
  word = word.toString().split('').reverse();
  for (let i = 3; i < word.length; i += 3) {
    word[i] += ',';
  }
  return '$' + word.reverse().join('');
};