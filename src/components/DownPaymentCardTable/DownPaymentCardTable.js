import React from 'react';
import './DownPaymentCardTable.css';

const DownPaymentCardTable = ({listItem, visible, plusMinusClick, totalIncomeVisible, expensesVisible, mortVisible}) => {
  // there are ids for set false or true value to state of DownPaymentCard for show or hide block with info
  return (
    <table className={`DownPaymentCardTable ${visible === 'open' ? 'Active' : ''}`}>
      <tbody>
      <tr className="green__color">
        <td colSpan="2"><p>Annual Total inc</p>
          <span id="totalIncome" onClick={plusMinusClick} className="plus">+</span>
          <span id="totalIncome-" onClick={plusMinusClick} className="minus">-</span>
        </td>
        <td colSpan="2">{listItem.gsx$incperyear.$t}</td>
      </tr>
      <tr className={`${totalIncomeVisible ? '' : 'TableHidden'}`}>
        <td>Res</td>
        <td>{listItem.gsx$res.$t}</td>
        <td>Com</td>
        <td>{listItem.gsx$com.$t}</td>
      </tr>
      <tr className={`${totalIncomeVisible ? '' : 'TableHidden'}`}>
        <td>Laundry</td>
        <td>{listItem.gsx$laundry.$t}</td>
        <td>Parking</td>
        <td>{listItem.gsx$parking.$t}</td>
      </tr>
      <tr className="green__color">
        <td colSpan="2"><p>Expenses</p>
          <span id="expenses" onClick={plusMinusClick} className="plus">+</span>
          <span id="expenses-" onClick={plusMinusClick} className="minus">-</span>
        </td>
        <td colSpan="2">{listItem.gsx$exp.$t}</td>
      </tr>
      <tr className={`${expensesVisible ? '' : 'TableHidden'}`}>
        <td  className="Asterisk">
          Vacancy 3% *
          <div className="TableBubble">
            <p>3% vacancy applied on properties over 10 units</p>
          </div>
        </td>
        <td>{listItem.gsx$vac3.$t}</td>
        <td  className="Asterisk">
          Mgmt 5% **
          <div className="TableBubble">
            <p>5% total income mgmt fee applied on properties over 10 units</p>
          </div>
        </td>
        <td>{listItem.gsx$mgmt.$t}</td>
      </tr>
      <tr className={`${expensesVisible ? '' : 'TableHidden'}`}>
        <td>Taxes</td>
        <td>{listItem.gsx$taxes.$t}</td>
        <td>Water/Sewage</td>
        <td>{listItem.gsx$watertax.$t}</td>
      </tr>
      <tr className={`${expensesVisible ? '' : 'TableHidden'}`}>
        <td>Energy</td>
        <td>{listItem.gsx$energy.$t}</td>
        <td className="Asterisk">
          Reserve(500/unit) *
          <div className="TableBubble">
            <p>500$/ unit as per CMHC standards</p>
          </div>
        </td>
        <td>{listItem.gsx$reserve.$t}</td>
      </tr>
      <tr className={`${expensesVisible ? '' : 'TableHidden'}`}>
        <td>Superintendent</td>
        <td>{listItem.gsx$super.$t}</td>
        <td>Misc</td>
        <td>{listItem.gsx$misc.$t}</td>
      </tr>
      <tr className="green__color">
        <td  className="Asterisk" colSpan="2"><p>Annual Mort *</p>
          <span id="mort" onClick={plusMinusClick} className="plus">+</span>
          <span id="mort-" onClick={plusMinusClick} className="minus">-</span>
          <div className="TableBubble">
            <p>Based on 3.4% interest (hyperlink to bank or banks), 25 years amortization</p>
          </div>
        </td>
        <td colSpan="2">{listItem.gsx$mortgage.$t}</td>
      </tr>
      <tr className={`${mortVisible ? '' : 'TableHidden'}`}>
        <td>Principal</td>
        <td>{listItem.gsx$principal.$t}</td>
        <td>Interest</td>
        <td>{listItem.gsx$interest.$t}</td>
      </tr>
      <tr className="green__color">
        <td colSpan="2"><p>Cash Flow/year</p>
        </td>
        <td colSpan="2">{listItem.gsx$cashflow.$t}</td>
      </tr>
      {/*<tr className="green__color">*/}
      {/*  <td colSpan="2">Cash Flow/month</td>*/}
      {/*  <td colSpan="2">{listItem.gsx$permonth.$t}</td>*/}
      {/*</tr>*/}
      <tr>
        <td>Return on Cash<br/>
          Flow = Cash<br/>
          Flow/DownP
        </td>
        <td>{listItem.gsx$roi.$t}</td>
        <td>Total Yield<br/>
          (Cash flow and equity)
        </td>
        <td>{listItem.gsx$totalyeild.$t}</td>
      </tr>
      </tbody>
    </table>
  );
};

export default DownPaymentCardTable;