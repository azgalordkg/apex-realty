import React from 'react';
import Header from "../LayoutComponents/Header";
import ContactUs from "../LayoutComponents/ContactUs";

const Layout = ({children}) => (
  <div className="Layout wrapper">
    <Header/>
    {children}
    <ContactUs/>
  </div>
);

export default Layout;