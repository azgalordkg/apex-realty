import React from 'react';
import Container from "../../UI/Container/Container";
import './TableBlock.css';

import CanadaFlag from '../../../assets/images/canada_flag.png';
import CardsPreloader from "../../UI/CardsPreloader/CardsPreloader";

const TableBlock = ({dropdownOpenClick, isDropdownVisible, data}) => {
  if (!data) {
    return <CardsPreloader styles={{margin: '60px 0'}}/>
  }
  return (
    <div className="table__block">
      <Container>
        <table>
          <thead>
          <tr>
            <th className="d-flex flex-column justify-content-center align-items-center TableBlockDropdownContainer">
              Total buildings in <br/>
              <span className="d-flex align-content-center mt5px">
                  <img className="flag_icon" src={CanadaFlag} alt=""/>
                  CANADA
                  <span onClick={dropdownOpenClick} className="carret">
                      <i/>
                  </span>
              </span>
              <div className={`TableBlockDropdown ${isDropdownVisible ? 'Active' : ''}`}>
                <span className="UsaComing">US coming soon</span>
              </div>
            </th>
            <th>AAA<br/>ROI +8%</th>
            <th>AA<br/>ROI 4-8%</th>
            <th>A<br/>ROI 0-4%</th>
            <th>Cash Flow <br/> Neg</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            {data.map((dataItem, index) => (
              <td key={index}>{dataItem.gsx$_cokwr.$t}</td>
            ))}
          </tr>
          <tr>
            <td>Market Share</td>
            {data.map((dataItem, index) => (
              index !== 0 ? <td key={index}>{dataItem.gsx$marketshare.$t}</td> : null
            ))}
          </tr>
          </tbody>
        </table>
      </Container>
    </div>
  )
};

export default TableBlock;