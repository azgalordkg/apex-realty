import React from 'react';

const HeroBlock = () => (
  <div style={{backgroundImage: `url`}} className="hero__block">
    <div className="container">
      <p className="text__red">Invest <span className="line__red"/></p>
      <p className="fs150p">WISELY</p>
      <p className="header__blockslogan" style={{fontSize: '19px'}}>Find the best of the best in real estate investment <br/> based on rate of return (ROI)</p>
    </div>
  </div>
);

export default HeroBlock;