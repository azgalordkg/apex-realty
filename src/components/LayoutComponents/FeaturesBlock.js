import React from 'react';

import ConsultationIcon from '../../assets/images/consultation_icon.png';
import DailyIcon from '../../assets/images/daily_updates_icon.png';
import EfficientIcon from '../../assets/images/efficient_icon.png';
import MakeBreakIcon from '../../assets/images/make_break_icon.png';
import NotifyIcon from '../../assets/images/notify_icon.png';
import StandIcon from '../../assets/images/stand_out_icon.png';

const FeaturesBlock = () => (
  <div id="Features" className="features__block">
    <div className="container">
      <div id="Investor" className="row__block row__block_left">
        <div className="left_block">
          <div className="heading_block">
            Investor
            <div className="red__dash"/>
          </div>
          <div className="feature__item">
            <div className="icon__block">
              <img src={MakeBreakIcon} alt=""/>
            </div>
            <div className="descripion__block">
              <div className="heading"> Make or Break</div>
              <p>We only showcase the best. Properties that make a profit or
                break
                even is the name of this game.</p>
            </div>
          </div>
          <div className="feature__item">
            <div className="icon__block">
              <img src={EfficientIcon} alt=""/>
            </div>
            <div className="descripion__block">
              <div className="heading">Efficient Search</div>
              <p>Don’t waste your time browsing properties that
                will cost you money at the end of the month. This makes our service extremely efficient.
                At
                ARI you have eliminated an overwhelming number of
                revenue properties based on their profitability.</p>
            </div>
          </div>
          <div className="feature__item">
            <div className="icon__block">
              <img src={DailyIcon} alt=""/>
            </div>
            <div className="descripion__block">
              <div className="heading">Daily Updates</div>
              <p>Every day, we go through all property incomes, expenses and vacancies so you won’t have
                to.
                The math is already done. The rest is up to you.</p>
            </div>
          </div>
        </div>
      </div>
      <div id="Owner" className="row__block row__block_right">
        <div className="heading_block">
          Owner
          <div className="red__dash"/>
        </div>
        <div className="feature__item">
          <div className="icon__block">
            <img src={ConsultationIcon} alt=""/>
          </div>
          <div className="descripion__block">
            <div className="heading">Complementary Consultation</div>
            <p>To sell a revenue property, we think like a real estate investor. If a property is at a loss,
              the investment isn’t attractive to any of our subscribed investors.</p>
          </div>
        </div>
        <div className="feature__item">
          <div className="icon__block">
            <img src={NotifyIcon} alt=""/>
          </div>
          <div className="descripion__block">
            <div className="heading">Notify Investors</div>
            <p>By enlisting your revenue property with ARI, our
              subscribed investors will be notified of your
              property.</p>
          </div>
        </div>
        <div className="feature__item">
          <div className="icon__block">
            <img src={StandIcon} alt=""/>
          </div>
          <div className="descripion__block">
            <div className="heading">Stand Out</div>
            <p>With the information on your property verified, ARI can proudly highlight your property by
              providing premium features like mapping and photos.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default FeaturesBlock;