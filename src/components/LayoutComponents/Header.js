import React from 'react';
import Container from "../UI/Container/Container";
import AnchorLink from 'react-anchor-link-smooth-scroll';

import UKFlag from '../../assets/images/flags/UK.svg';
import FRFlag from '../../assets/images/flags/FR.svg';
import Logo from '../../assets/images/apex_logo.png';

const Header = () => {
  return (
    <div className="Header">
      <Container>
        <div id="home" className="header__block">
          <div className="logo">
            <img src={Logo} alt=""/>
          </div>
          <div className="menu">
            <ul>
              <li><AnchorLink className="HeaderLink" offset='100' href='#home'>Home</AnchorLink></li>
              <li><AnchorLink className="HeaderLink" href='#howItWorks'>How it works</AnchorLink></li>
              <li><AnchorLink className="HeaderLink" href="#Investor">Investor</AnchorLink></li>
              <li><AnchorLink className="HeaderLink" href="#Owner">Owner</AnchorLink></li>
              <li><AnchorLink className="HeaderLink" href="#Contacts">Contact</AnchorLink></li>
            </ul>
          </div>
          <div className="drop-down">
            <select name="options">
              <option className="" value="1" style={{backgroundImage: `url(${UKFlag})`, backgroundRepeat: 'no-repeat', backgroundSize: '30px'}}>
                &nbsp;
              </option>
              <option className="" value="1" style={{backgroundImage: `url(${FRFlag})`, backgroundRepeat: 'no-repeat', backgroundSize: '30px'}}>
                &nbsp;
              </option>
            </select>
          </div>
          <div className="mobile__menu__icon">
            <span/>
            <span/>
            <span/>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Header;