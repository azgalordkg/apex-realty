import React from 'react';

import AddressIcon from '../../assets/images/address__icon.png';
import EmailIcon from '../../assets/images/email__icon.png';
import PhoneIcon from '../../assets/images/phone__icon.png';

const ContactUs = () => (
  <div id="Contacts" className="contatct__us">
    <div className="container">
      <div className="contact__item address__block">
        <img src={AddressIcon} alt=""/>
        <div className="d-flex">
          <p className="grey__color">Address:</p>
          <p>
            Royal LePage Altitude<br/>
            1215 Notre Dame West
          </p>
        </div>
      </div>
      <div className="contact__item phone__block">
        <img src={PhoneIcon} alt=""/>
        <div className="d-flex">
          <p className="grey__color">Phone:</p>
          <p className="font__bold">
            514-846-090
          </p>
        </div>
      </div>
      <div className="contact__item email__block">
        <img src={EmailIcon} alt=""/>
        <div className="d-flex">
          <p className="grey__color ">Email:</p>
          <p className="color__red">
            antoinesaker@me.com
          </p>
        </div>
      </div>
    </div>
    <p className="copy__right">© 2019
      <span style={{padding: '0 10px', display: 'inline-block'}}
            className="color__red">
        Apex Realty Investments.
      </span>
      All Right Reserved.
    </p>
  </div>
);

export default ContactUs;