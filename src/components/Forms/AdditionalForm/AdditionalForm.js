import React from 'react';

import './AdditionalForm.css';

const AdditionalForm = ({values, change, disabled, onSubmit}) => (
  <div className="AdditionalForm">
    <h2>Property Inquiry</h2>
    <div className="red__dash"/>
    <span>{values.units} Units in {values.area}</span>
    <form>
      <input
        type="name" name="firstName"
        placeholder="First Name"
        value={values.firstName} onChange={change}
      />
      <input
        type="name" name="lastName"
        placeholder="Last Name"
        value={values.lastName} onChange={change}
      />
      <input
        type="email" name="email"
        placeholder="Email"
        value={values.email} onChange={change}
      />
      <input
        type="phone" name="phone"
        placeholder="Telephone"
        value={values.phone} onChange={change}
      />
      <button disabled={disabled} onClick={onSubmit}>
        <span>Submit Now</span>
      </button>
    </form>
  </div>
);

export default AdditionalForm;