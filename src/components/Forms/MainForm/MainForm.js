import React from 'react';
import './MainForm.css';
import Checkbox from 'react-simple-checkbox';

const MainForm = ({values, change, onCheckboxChange, onSubmit}) => (
  <div className="MainForm about__selling">
    <div className="container">
      <div className="heading">Thinking <span> About Selling</span></div>
      <div className="red__dash"/>
      <p>Please answer the following and a representative will contact you shortly.</p>
      <div className="form__block">
        <form>
          <div className="col__wrapper">
            <div className="form__col">
              <input
                type="name" name="name"
                onChange={change} value={values.name}
                placeholder="Full Name"
              />
              <input
                type="text" name="units"
                onChange={change} value={values.units}
                placeholder="Units"
              />
              <input
                type="text" name="income"
                onChange={change} value={values.income}
                placeholder="$ Income"
              />
              <input
                type="email" name="email"
                onChange={change} value={values.email}
                placeholder="Email"
              />
              <input
                type="text" name="price"
                onChange={change} value={values.price}
                placeholder="$ Price"
              />
              <div>
                <div className="CheckboxContainer">
                  <Checkbox onChange={onCheckboxChange} borderThickness={1} size={3} color={'#000'}/>
                  <span>Fully Rented</span>
                </div>
              </div>
            </div>
          </div>
          <div className="btn__row">
            <button
              disabled={values.name.length === 0 || values.email.length === 0 // checking if one value in form is empty and it it's true we make button disabled
                || values.units.length === 0 || values.price.length === 0
                || values.income.length === 0}
              onClick={onSubmit} className="sumit"
            >
              Submit Now
              <i className="fa fa-long-arrow-right" aria-hidden="true"/>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
);

export default MainForm;