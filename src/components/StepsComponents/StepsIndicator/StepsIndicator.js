import React, {Fragment} from 'react';

import './StepsIndicator.css';

import Step1 from '../../../assets/images/step_1.png';
import Step2 from '../../../assets/images/step_2.png';
import Step3 from '../../../assets/images/step_3.png';

const steps = [
  {number: 1, title: 'Step 1', text: 'Location', icon: Step1},
  {number: 2, title: 'Step 2', text: 'Downpayment', icon: Step2},
  {number: 3, title: 'Step 3', text: 'Units', icon: Step3},
];

// Function for returning width of StepsIndicatorLineInner

const returnLengthOfLine = step => {
  if (step === 2) return '50%';
  if (step === 3) return '100%';
  return '0';
};

const StepsIndicator = ({activeStep}) => {
  return (
    <div className="StepsIndicator">
      <div className="StepsIndicatorLine">
        <div style={{width: returnLengthOfLine(activeStep)}} className="StepsIndicatorLineInner"/>
        {steps.map(step => (
          <Fragment key={step.number}>
            <img className={`StepsIndicatorIcon StepsIndicatorIcon${step.number}`} src={step.icon} alt=""/>
            <div
              style={{borderColor: step.number <= activeStep ? '#62b93b' : '#f27373'}}
              className={`StepsIndicatorItemCircle StepsIndicatorItemCircle${step.number}`}>
              <div style={{background:step.number <= activeStep ? '#62b93b' : '#f27373'}}>
                <span>{step.number}</span>
              </div>
            </div>
            <div className={`StepsIndicatorItemText StepsIndicatorItemText${step.number}`}>
              <h5>{step.title}</h5>
              <p>{step.text}</p>
            </div>
          </Fragment>
        ))}
      </div>
    </div>
  );
};

export default StepsIndicator;