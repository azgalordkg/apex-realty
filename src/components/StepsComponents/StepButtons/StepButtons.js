import React from 'react';

import './StepButtons.css';

const StepButtons = ({prevClick, nextClick, findClick, step, ifDisabled, update}) => {
  return (
    <div style={{'justifyContent' : step > 1 ? 'space-between' : 'flex-end'}}
         className="StepButtons btn__row">
      <button
        style={{display: step > 1 ? 'block' : 'none'}}
        onClick={prevClick}
      >
        Previous
      </button>
      <button
        disabled={ifDisabled}
        onClick={step < 3 ? nextClick : findClick}
        className="next__button"
        style={
          step === 3 ? {width : '300px', backgroundPosition: '260px center'} : null
        }
      >
        {(!update || step < 3) ? step < 3 ? 'Next' : 'Find Properties' : 'Update results'}
        <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
      </button>
    </div>
  );
};

export default StepButtons;