import React from 'react';

import './StepDropDowns.css';
import MultiSelectReact from "multi-select-react";
import {getValueWithCommas} from "../../../constants";

const selectedOptionsStyles = {backgroundColor: "#dff0d8", color: "#3c763d"};
const optionsListStyles = {backgroundColor: "#dff0d8", color: "#3c763d"};

const StepDropDowns = ({steps, selected, change,
                         optionClicked, selectedBadgeClicked,
                         multiSelect1, multiSelect2, multiSelectUnits,
                         optionClickedUnits, selectedBadgeClickedUnits}) => {
  return (
    <div className="StepDropDowns select__wrapper">
      {steps.map((step, index) => {
        if (steps.length > 1 || step.name === 'Units') {
          return (
            <div
              style={{width: step.name === 'Units' ? '100%' : null}}
              key={index} className="StepDropDownsItemMulti">
              <span className="MultiSelectLabel">{step.name}</span>
              <MultiSelectReact
                options={
                  step.name !== 'Units' // here we checking if this component renders for First step or for Third step
                  ? (index === 0 ? multiSelect1 : multiSelect2)
                  : multiSelectUnits
                }
                optionClicked={step.name !== 'Units' ? optionClicked(index) : optionClickedUnits} // here we checking if this component renders for First step or for Third step
                selectedBadgeClicked={step.name !== 'Units' ? selectedBadgeClicked(index) : selectedBadgeClickedUnits} // here we checking if this component renders for First step or for Third step
                selectedOptionsStyles={selectedOptionsStyles}
                optionsListStyles={optionsListStyles}
              />
            </div>
          )
        } else {
          return (
            <div className="StepDropDownsItem"
                 key={index}>
              <span>{selected[step.name] !== 'Max Downpayment' && selected[step.name] !== 'All' ? getValueWithCommas(selected[step.name]) : selected[step.name]}</span>
              <select
                className="custom-select" value=""
                onChange={change} name={step.name}>
                {step.content.map((value, index) => <option key={index}>{value}</option>)}
              </select>
              <div className="SelectDropArrow">
                <div className="SelectDropArrowTriangle"/>
              </div>
            </div>
          )
        }
      })}
    </div>
  );
};

export default StepDropDowns;