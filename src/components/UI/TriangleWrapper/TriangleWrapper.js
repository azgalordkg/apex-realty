import React from 'react';
import {returnAAA,} from "../../../constants";

import './TriangleWrapper.css';

import HandlerIcon from '../../../assets/images/slider__handler.png';

// Function for change position of SliderHandler

const changePosition = roi => {
  const thirdPart = 33.3333;
  // roi is a value for position of SliderHandler in Triangle
  if (roi) {
    const roiNumber = parseFloat(roi.split('').map(value => value === '%' ? '' : value).join(''));
    if (roiNumber >= 0 && roiNumber <= 4) {
      const A = 99 - thirdPart / 4 * roiNumber;
      if (A === 100) return '98%';
      return A + '%';
    }
    if (roiNumber > 4 && roiNumber <= 6) {
      return (132.3333 - thirdPart / 2 * roiNumber) + '%';
    }
    if (roiNumber > 6) {
      const AAA = 133.3333 - thirdPart / 2 * roiNumber;
      if (AAA < 1) return '2%';
      return AAA + '%';
    }
  }
  return '98%';
};

const TriangleWrapper = ({fixed, roi, absolutePosition}) => (
  <div style={{
    position: fixed ? 'fixed' : null,
    top: absolutePosition ? absolutePosition : (fixed ? '40px' : null)
  }}
       id="triangle"
       className="TriangleWrapper triangle__wrapper">
    <div className="triangle__block">
      <div style={{top: changePosition(roi)}} className="SliderHandler">
        <p className="SliderHandlerROI">{returnAAA(roi)}</p>
        <span>
          <img src={HandlerIcon} alt=""/>
        </span>
        <p className="SliderHandlerPercent">{roi}</p>
      </div>
    </div>
  </div>
);

export default TriangleWrapper;