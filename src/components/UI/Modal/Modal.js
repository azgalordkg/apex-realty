import React from 'react';

import './Modal.css';

const Modal = ({children, visible, closeModal}) => (
  <div id="ModalBackground" className={`ModalBackground ${visible ? 'Open' : ''}`}>
    <div className= "Modal">
      {children}
      <button id="ModalButton" onClick={closeModal} className="ModalClose">X</button>
    </div>
  </div>
);

export default Modal;