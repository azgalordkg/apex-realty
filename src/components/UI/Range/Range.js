import React from 'react';
import InputRange from "react-input-range";
import 'react-input-range/lib/css/index.css';

const Range = ({ value, change }) => {
  return (
    <div>
      <h2>Range</h2>
      <InputRange
        formatLabel={value => `$${value}`}
        minValue={25000}
        maxValue={1500000}
        value={value}
        onChange={change}
      />
    </div>
  );
};

export default Range;