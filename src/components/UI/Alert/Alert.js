import React from 'react';
import './Alert.css';

const successSvg = (
  <svg className="j2dfb39 j1ifr0zx jyh5bfp" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
       role="presentation">
    <path fill="none" d="M0 0h24v24H0z"/>
    <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"/>
  </svg>
);

const errorSvg = (
  <svg className="j2dfb39 j1ifr0zx jyh5bfp" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
    <path fill="none" d="M0 0h24v24H0z"/>
    <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z"/>
  </svg>
);

const Alert = ({text, click, active, type}) => (
  <div className={`Alert ${active ? 'Active' : null} ${type}`}>
    <div className="AlertContent">
      {type === 'Success' ? successSvg : errorSvg}
      <p>{text}</p>
      <button onClick={click} className="jr0u89w j1g2vga5 j5pbwdt" tabIndex="0" type="button" aria-label="Close">
        <span className="jsbmc5e">
          <svg className="j2dfb39 j1ifr0zx" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
            <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
            <path fill="none" d="M0 0h24v24H0z"/>
          </svg>
        </span>
        <span className="jb3bkca"/>
      </button>
    </div>
  </div>
);

export default Alert;