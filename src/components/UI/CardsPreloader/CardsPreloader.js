import React from 'react';

import './CardsPreloader.css';

const CardsPreloader = ({styles}) => {
  return (
    <div style={styles} className="CardsPreloader">
      <div className="item-1"/>
      <div className="item-2"/>
      <div className="item-3"/>
      <div className="item-4"/>
      <div className="item-5"/>
    </div>
  );
};

export default CardsPreloader;