import React from 'react';

import './Calculator.css';

const percentInputs = ['valuePercent1', 'valuePercent2', 'valuePercent3', 'valuePercent4'];

const sumInputs = ['valueSum1', 'valueSum2', 'valueSum3', 'valueSum4'];

const mortgages = ['mortgage1', 'mortgage2','mortgage3','mortgage4'];

const Calculator = props => {
  return (
    <div className="Calculator">
      <div className="CalculatorHead">
        <h2>Asking price</h2>
        <input type="text" value={props.valueAsking}
               name={props.valueAsking} onChange={props.change}/>
        <button onClick={props.clickGo}>Go</button>
      </div>
      <div className="CalculatorHeadDownPayment">
        <h2>Down payment</h2>
        <div className="CalculatorHeadDownPaymentInputs">
          {percentInputs.map(percentInput => (
            <input
              key={percentInput} onBlur={props.blur}
              type="text" value={props[percentInput]}
              onChange={props.change} name={percentInput}/>
          ))}
          {sumInputs.map(sumInput => (
            <input
              key={sumInput}
              type="text" value={props[sumInput]}
              onChange={props.change} name={sumInput}/>
          ))}
        </div>
      </div>
      <div className="CalculatorHeadDownPaymentMortgage">
        <h2>Mortgage insurance</h2>
        <div className="CalculatorHeadDownPaymentMortgageText">
          {mortgages.map(mortgage => (
            <p key={mortgage}>{props[mortgage]}</p>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Calculator;