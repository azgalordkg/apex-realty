import axios from 'axios';

const axiosApex = axios.create({
  baseURL: 'https://spreadsheets.google.com/feeds/list/1jiNEVoNu_OuX0riCeabirF2Bo2jFfV6nbLkfOuSEvpo/odkkh6n/public/values?alt=json'
});

export default axiosApex;