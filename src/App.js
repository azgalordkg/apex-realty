import React, {Component} from 'react';
import {connect} from "react-redux";
import './App.css';
import axios from 'axios';
import {mainForm, additionalForm} from "./constants";

import DownPaymentCards from "./containers/DownPaymentCards/DownPaymentCards";
import Modal from "./components/UI/Modal/Modal";
import AdditionalForm from "./components/Forms/AdditionalForm/AdditionalForm";
import HowItWorks from "./containers/HowItWorks/HowItWorks";
import Layout from "./components/Layout/Layout";
import HeroBlock from "./components/LayoutComponents/HeroBlock";
import TableBlock from "./components/LayoutComponents/TableBlock/TableBlock";

import MainForm from "./components/Forms/MainForm/MainForm";
import FeaturesBlock from "./components/LayoutComponents/FeaturesBlock";
import Alert from "./components/UI/Alert/Alert";
// import Range from "./components/UI/Range/Range";
// import Calculator from "./components/Calculator/Calculator";

class App extends Component {
  state = {
    value: 1500000,
    mainForm: mainForm,
    additionalForm: additionalForm,
    modal: false,
    alertText: '',
    alertActive: false,
    alertType: 'Success',
    countryDropdownOpen: false,
    tableTotalBuildings: null,
    // calculator: {
    //   valueAsking: '$ 520,000',
    //   valuePercent1: '5.19 %',
    //   valuePercent2: '10.00 %',
    //   valuePercent3: '15.00 %',
    //   valuePercent4: '20.00 %',
    //   valueSum1: '',
    //   valueSum2: '',
    //   valueSum3: '',
    //   valueSum4: '',
    //   mortgage1: '',
    //   mortgage2: '',
    //   mortgage3: '',
    //   mortgage4: '',
    // }
  };

  componentDidMount() {
    // this.getLabelsForRange();
    // this.getDownPaymentSums();
    this.getDataTotalBuildings();
  }

  // Function for getting label From and To for Range on page
  //
  // getLabelsForRange = () => {
  //   const min = document.getElementsByClassName('input-range__label input-range__label--min');
  //   const max = document.getElementsByClassName('input-range__label input-range__label--max');
  //   const newSpan = document.createElement('span');
  //   min[0].prepend(newSpan.innerHTML = 'From ');
  //   max[0].prepend(newSpan.innerHTML = 'To ');
  // };
  //
  //
  // //Functions for getting input values in percents
  //
  // getPercentsInputValue = value => {
  //   if (value) {
  //     let dot = false;
  //     value.split('').forEach(valueItem => {
  //       if (valueItem === '.') dot = true;
  //     });
  //     if (Number.isInteger(parseFloat(value)) || !dot) {
  //       value = parseFloat(value) + '.00';
  //     }
  //     if (!isNaN(value)) {
  //       return value += ' %';
  //     }
  //   }
  //   return value;
  // };
  //
  // calculatorBlurHandler = event => {
  //   const calculator = {...this.state.calculator};
  //   calculator[event.target.name] = this.getPercentsInputValue(event.target.value);
  //   this.setState({calculator});
  //   this.getDownPaymentSums();
  // };
  //
  // // Function for changing input values in Calculator
  //
  // calculatorChangeHandler = event => {
  //   this.setState({
  //     calculator: {
  //       ...this.state.calculator,
  //       [event.target.name]: event.target.value,
  //     }
  //   });
  // };
  //
  // // Function for getting Down Payment sums
  //
  // getDownPaymentSums = () => {
  //   const calculator = {...this.state.calculator};
  //   const valueAsking = parseFloat(this.state.calculator.valueAsking
  //     .split('')
  //     .map(value => (value === '$' || value === ',') ? '' : value)
  //     .join('')
  //   );
  //
  //   for (let i = 1; i < 5; i++) {
  //     const percent = parseFloat(this.state.calculator[`valuePercent${i}`]);
  //     calculator[`valueSum${i}`] = Math.round(valueAsking * (percent * 0.01) / 100) * 100;
  //   }
  //   this.setState({calculator});
  // };
//
// <Calculator
// valueAsking={this.state.calculator.valueAsking}
// change={this.calculatorChangeHandler}
// blur={this.calculatorBlurHandler}
//
// valuePercent1={this.state.calculator.valuePercent1}
// valuePercent2={this.state.calculator.valuePercent2}
// valuePercent3={this.state.calculator.valuePercent3}
// valuePercent4={this.state.calculator.valuePercent4}
//
// valueSum1={this.state.calculator.valueSum1}
// valueSum2={this.state.calculator.valueSum2}
// valueSum3={this.state.calculator.valueSum3}
// valueSum4={this.state.calculator.valueSum4}
// />
  // <Range
// value={this.state.value}
// change={value => this.setState({value})}/>

  // Function for get value for Total Buildings Table from API

  getDataTotalBuildings() {
    axios.get('https://spreadsheets.google.com/feeds/list/1rCBuTE6vf5jrw0btz3FMfR75rZKgLk6zB0UTpSO5WLE/od6/public/values?alt=json')
      .then(response => {
        this.setState({tableTotalBuildings: response.data.feed.entry});
      });
  }

  // Function for change values in forms

  formChangeHandler = (event, formName) => {
    // formName - name of key of form in state
    const form = {...this.state[formName]};
    form[event.target.name] = event.target.value;
    this.setState({[formName]: form});
  };

  // Function for changing checkbox

  checkboxChangeHandler = boolean => {
    // boolean can be true or false
    const mainForm = {...this.state.mainForm};
    mainForm.fullyRented = boolean;
    this.setState({mainForm});
  };

  // Function for submit form

  submitForm = (event, formName) => {
    // formName - name of key of form in state
    event.preventDefault();
    const form = {...this.state[formName]};
    let text = '';
    let subject = '';

    if (formName === 'mainForm') {
      text = `Name: ${form.name} | Email: ${form.email} | # of Units: ${form.units} 
            | Annual Rental Income ${form.income} | Price: ${form.price} | Fully Rented: ${form.fullyRented ? 'YES' : 'NO'}`;
      subject = 'Selling Form';
    } else if (formName === 'additionalForm') {
      text = `Name: ${form.firstName} ${form.lastName} | Email: ${form.email} | Phone: ${form.phone}`;
      subject = 'Inquiry | MLS ' + form.mls;
    }

    const message = {
      from_email: "apex@noqueue.ca",
      from_name: "APEX Realty Investments",
      subject,
      text,
      to: [{email: "antoinesaker@me.com", name: "Antoine Saker"}],
    };
    console.log(message);

    const request = {
      async: false,
      ip_pool: null,
      key: "fY_fWfMMFANaeLYBCIEhhg",
      message,
      send_at: null,
    };

    this.setState({additionalForm: additionalForm, mainForm: mainForm,});
    if (formName === 'additionalForm') this.toggleModal();

    axios.post('https://mandrillapp.com/api/1.0/messages/send.json', request).then(
      res => {
        this.setState({alertActive: true, alertText: 'The form has been submit!', alertType: 'Success'});
        setInterval(() => this.setState({alertActive: false}), 3000);
      },
      error => {
        this.setState({alertActive: true, alertText: 'Something went wrong!', alertType: 'Error'});
        setInterval(() => this.setState({alertActive: false}), 3000);
      }
    )
  };

  // Function for sending form after click on Inquire button

  openModalWithForm = (event, listItem) => {
    const additionalForm = {...this.state.additionalForm};
    additionalForm.units = listItem.gsx$units.$t;
    additionalForm.area = listItem.gsx$area.$t;
    additionalForm.mls = listItem.gsx$mls.$t;

    this.setState({additionalForm});
    this.toggleModal();
  };

  // Function for toggle Modal

  toggleModal = () => {
    this.setState({modal: !this.state.modal});
  };

  // Function for checking if fields in AdditionalForm are empty

  checkFieldsInAdditionalForm = () => {
    return (this.state.additionalForm.firstName === ''
      || this.state.additionalForm.lastName === ''
      || this.state.additionalForm.email === ''
      || this.state.additionalForm.phone === '') ;
  };

  render() {
    return (
      <div className="App">
        <Layout>
          <HeroBlock/>
          <TableBlock
            dropdownOpenClick={() => this.setState({countryDropdownOpen: !this.state.countryDropdownOpen})}
            isDropdownVisible={this.state.countryDropdownOpen} data={this.state.tableTotalBuildings}
          />
          <HowItWorks/>
          <DownPaymentCards openModalWithForm={this.openModalWithForm}/>
          <FeaturesBlock/>
          <MainForm
            values={this.state.mainForm}
            change={event => this.formChangeHandler(event, 'mainForm')}
            onSubmit={event => this.submitForm(event, 'mainForm')}
            onCheckboxChange={this.checkboxChangeHandler}
          />
        </Layout>
        <Modal visible={this.state.modal} closeModal={this.toggleModal}>
          <AdditionalForm
            values={this.state.additionalForm} // values of Form from state
            change={event => this.formChangeHandler(event, 'additionalForm')}
            disabled={this.checkFieldsInAdditionalForm()}
            onSubmit={event => this.submitForm(event, 'additionalForm')}
          />
        </Modal>
        <Alert
          active={this.state.alertActive}
          text={this.state.alertText}
          type={this.state.alertType}
          click={() => this.setState({alertActive: false})}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  sorted: state.sortedList,
});

export default connect(mapStateToProps)(App);
